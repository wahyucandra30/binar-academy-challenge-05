import DefaultButton from "./elements/DefaultButton";
import formatIDR from "./utils/FormatIDR";
import { useSelector } from "react-redux"

const CarCard = ({ data }) => {
    const { capacity, transmission, productionYear } = useSelector((state) => state.carDataReducer?.carSpecs);
    return (
        <div className="border flex flex-col justify-center items-center bg-white 
            w-full md:w-80 min-h-fit rounded-lg shadow-sm shadow-slate-300 p-6">
            <img src={data.image} alt="Car Preview" className="w-[270px] h-[160px]" />
            <div className="text-base text-left">
                <h2 className="mt-8">{data.name}</h2>
                <h1 className="font-bold mt-2">{formatIDR(data.price)} / hari</h1>
                <p className="text-sm mt-2">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                </p>
                <div className="flex flex-col gap-2 text-sm font-normal mt-2">
                    <div className="flex items-center gap-2">
                        <img src="./icon_users.svg" alt="" />
                        {capacity} Orang
                    </div>
                    <div className="flex items-center gap-2">
                        <img src="./icon_gear.svg" alt="" className="w-[18px]" />
                        {transmission}
                    </div>
                    <div className="flex items-center gap-2">
                        <img src="./icon_calendar.svg" alt="" />
                        Tahun {productionYear}
                    </div>
                </div>
                <div className="flex flex-col w-full mt-6">
                    <DefaultButton placeholder={"Pilih Mobil"} linkTo={`car/${data.id}`}
                        defaultWidth={"w-full"} defaultHeight={"h-10"} mdHeight={"md:h-[36px]"} 
                    />
                </div>
            </div>
        </div>
    )
}
export default CarCard;