import { Link } from "react-router-dom";

const DefaultButton = ({ placeholder, linkTo, action,
    bgColor, bgHoverColor, defaultWidth, defaultHeight, mdWidth, mdHeight }) => {
    const color = bgColor ? bgColor : "bg-limegreen-4";
    const hoverColor = bgHoverColor ? bgHoverColor : "hover:bg-limegreen-5";
    const width = defaultWidth ? defaultWidth : "w-fit";
    const height = defaultHeight ? defaultHeight : "h-fit"
    return (
        <Link to={linkTo? linkTo : ""} onClick={action? action : ""}>
            <button type="button" className={`text-white font-bold rounded-sm text-sm text-center items-center 
            ${color} ${hoverColor} ${width} ${height} ${mdWidth? mdWidth : ""} ${mdHeight? mdHeight : ""}`}>
                {placeholder ? placeholder : "Button"}
            </button>
        </Link>
    )
}

export default DefaultButton;